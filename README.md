# ntan-core-processor

#### 介绍
第三方插件引用驱动器，多插件快速变更

#### 软件架构
软件架构说明


#### 安装教程

1. 在项目中配置仓库属性，也就是码云路径，与 <dependencies> 标签平级
```
<repositories>
    <repository>
        <id>gitee-maven</id>
        <url>https://gitee.com/ntan520/ntan-core-processor/tree/master/release</url>
    </repository>
</repositories>
```

2. 在 <dependencies> 标签中添加依赖
```
<dependency>
    <groupId>com.ntan520.core</groupId>
    <artifactId>ntan-core-processor</artifactId>
    <version>1.0</version>
</dependency>
```

参照：https://blog.ntan520.com/article/10627

#### 使用说明

1. 实现ClientAbstract抽象类, 对接第三方SDK调用接口
2. 实现IProcessorConfig配置类, 可在service, IProcessor, client之间共享参数配置
3. 继承ProcessorService实现service, 可在service层实现统一处理逻辑
4. 实现ProcessorManager抽象类，可实现对外入口。Manager可添加@Component注解实现SpringBoot自动托管。亦可同过new的方式，实现手动创建，addProcessor方法为手动创建初始化处理器方法
5. 拓展ProcessorAbstract类，可根据具体SDK定制功能事件，亦可执行统一处理逻辑
6. 实现ProcessorAbstract抽象类或其拓展抽象类，可实现多功能处理器，处理不同业务。处理器类可通过实现manager参照构造函数并在构造函数上添加@Autowired注解，配合@Component类注解，可实现自动注入。亦可通过manager中addProcessor方法实现手动注入
#### 注意事项
1. 实现或继承相关类时，相关泛型类使用实现后的类或抽象类
2. 可添加中间层，实现二级功能驱动
