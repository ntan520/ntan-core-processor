package com.ntan520.core.processor;

import java.util.HashMap;
import java.util.Map;

import com.ntan520.core.processor.core.IProcessor;

/**
 * @author Nick Tan
 */
public class ProcessorFactory {

    protected Map<Class<? extends IProcessor>, IProcessor> processorMap = new HashMap<Class<? extends IProcessor>, IProcessor>();

    public void addProcessor(Class<? extends IProcessor> clazz, IProcessor processor) {
        processorMap.put(clazz, processor);
    }

    public IProcessor getProcessor(Class<? extends IProcessor> clazz) {
        return processorMap.get(clazz);
    }

    public Map<Class<? extends IProcessor>, IProcessor> getProcessorMap() {
        return processorMap;
    }

}
