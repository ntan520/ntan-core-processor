package com.ntan520.core.processor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ntan520.core.processor.core.IClient;
import com.ntan520.core.processor.core.IProcessor;
import com.ntan520.core.processor.core.IProcessorConfig;
import com.ntan520.core.processor.core.ProcessorAbstract;

/**
 * @author Nick Tan
 */
public abstract class ProcessorManager<C extends IClient, PC extends IProcessorConfig, P extends ProcessorAbstract<PC, C>, S extends ProcessorService<P, PC>> {

    private final static Logger logger = LoggerFactory.getLogger(ProcessorManager.class);

    protected C client;

    protected ProcessorFactory factory;

    protected S service;

    protected PC config;

    protected abstract PC initConfig();

    protected abstract S initService(ProcessorFactory factory, PC config);

    protected abstract C initClient(PC config);

    public ProcessorManager(PC config) {
        super();
        this.config = config;
        this.initFacotory();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public P addProcessor(Class<? extends P> processorClazz) {
        P processor = (P) factory.getProcessor(processorClazz);

        if (processor == null) {
            try {
                Class[] paramTypes = { this.getClass() };
                Object[] params = { this };
                Constructor<? extends IProcessor> con = processorClazz.getConstructor(paramTypes);
                processor = (P) con.newInstance(params);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                logger.error("处理器初始化失败", e);
            }
        }

        return processor;
    }

    protected void initFacotory() {
        factory = new ProcessorFactory();
        if (config == null) {
            config = this.initConfig();
        }
        client = this.initClient(config);
        service = this.initService(factory, config);
    }

    public C getClient() {
        return client;
    }

    public ProcessorFactory getFactory() {
        return factory;
    }

    public S getService() {
        return service;
    }

    public PC getConfig() {
        return config;
    }

}
