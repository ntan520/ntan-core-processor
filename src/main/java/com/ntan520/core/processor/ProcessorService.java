package com.ntan520.core.processor;

import com.ntan520.core.processor.core.IProcessor;
import com.ntan520.core.processor.core.IProcessorConfig;

/**
 * @author Nick Tan
 */
public class ProcessorService<P extends IProcessor, C extends IProcessorConfig> {

    protected ProcessorFactory factory;

    protected C config;

    public ProcessorService(ProcessorFactory factory, C config) {
        super();
        this.factory = factory;
        this.config = config;
    }

    protected C getConfig() {
        return config;
    }

    @SuppressWarnings("unchecked")
    public P getProcessor(Class<? extends P> processorClazz) {
        return (P) factory.getProcessor(processorClazz);
    }

}
