package com.ntan520.core.processor.core;

/**
 * @author Nick Tan
 */
public abstract class ClientAbstract<PC extends IProcessorConfig> implements IClient {

    protected PC config;

    public ClientAbstract(PC config) {
        super();
        this.config = config;
    }

}
