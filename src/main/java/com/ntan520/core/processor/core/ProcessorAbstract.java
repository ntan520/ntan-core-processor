package com.ntan520.core.processor.core;

import com.ntan520.core.processor.ProcessorFactory;
import com.ntan520.core.processor.ProcessorManager;
import com.ntan520.core.processor.ProcessorService;

/**
 * @author Nick Tan
 */
public abstract class ProcessorAbstract<PC extends IProcessorConfig, C extends IClient> implements IProcessor {

    protected ProcessorFactory factory;

    protected C client;

    protected PC config;

    public <P extends ProcessorAbstract<PC, C>, S extends ProcessorService<P, PC>> ProcessorAbstract(
            ProcessorManager<C, PC, P, S> manager) {
        super();
        this.factory = manager.getFactory();
        this.factory.addProcessor(this.getClass(), this);
        this.client = manager.getClient();
        this.config = manager.getConfig();
    }

    public C getClient() {
        return client;
    }

}
